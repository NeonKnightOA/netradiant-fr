NetRadiant
==========


Ceci est une traduction Française de NetRadiant.


Sources ouverts, éditeur de niveau multi-plateforme des jeux idtech (dérivé de Radiant)

# Getting the Sources

Les dernières sources sont disponibles depuis le répertoire git:
https://gitlab.com/xonotic/netradiant.git

Le client git peut être obtenu depuis le site web de Git:
http://git-scm.org

Pour obtenir une copie des sources en utlisant la ligne de commande du client git:
```
git clone https://gitlab.com/xonotic/netradiant.git
cd netradiant
```

Voir également https://gitlab.com/xonotic/netradiant/ pour naviguer parmi les sources, publications et autre.

# Dependencies

 * OpenGL
 * LibXml2
 * GTK2
 * GtkGLExt
 * LibJpeg
 * LibPng
 * ZLib

# Compilation

Ce projet utilise le flux de travail habituel de CMake:

`cmake -H. -Bbuild && cmake --build build -- -j$(nproc)`

## linux

```
cmake -H. -Bbuild -G "Unix Makefiles"
```

## msys2

`pacman -S --needed base-devel`

### 32 bit:

```
pacman -S --needed mingw-w64-i686-{toolchain,cmake,gtk2,gtkglext}
cmake -H. -Bbuild -G "MSYS Makefiles" -DGTK2_GLIBCONFIG_INCLUDE_DIR=/mingw32/lib/glib-2.0/include -DGTK2_GDKCONFIG_INCLUDE_DIR=/mingw32/lib/gtk-2.0/include
```

### 64 bit:

```
pacman -S mingw-w64-x86_64-{toolchain,cmake,gtk2,gtkglext}
cmake -H. -Bbuild -G "MSYS Makefiles" -DGTK2_GLIBCONFIG_INCLUDE_DIR=/mingw64/lib/glib-2.0/include -DGTK2_GDKCONFIG_INCLUDE_DIR=/mingw64/lib/gtk-2.0/include
```

## OS X:

```
brew install gtkglext
brew install Caskroom/cask/xquartz
brew link --force gettext
```

Autres détails de compilation
------------------------

options:
 * `DOWNLOAD_GAMEPACKS=ON`
   Téélcharge automatiquement les données du gamepack durant la première compilation
 * `RADIANT_ABOUTMSG="Custom build"`
   Un message est exposé dans le sujet du dialogue

targets:
 * `radiant`    Compile le coeur binaire de radiant
 * `modules`    Compile tous les modules (chaque module a sa propre cible comme source)
 * `plugins`    Compile toutes les extensions (chaque extension a sa propre cible comme source)
 * `game_packs` Télécharge les données du gamepack
 * `quake3`     Compile tous les outils de Quake3
   - `q3map2`     Compilateur de cartes Quake3
   - `q3data`
